package com.hemebiotech.analytics;
import java.util.ArrayList;
import java.util.Map;

public class Main {
	
	public static void main(String[] args){
		
		//Taking Symptoms from file and storing them in an array list

		ISymptomReader list = new ReadSymptomDataFromFile("symptoms.txt");	
	
		ArrayList<String> result = (ArrayList<String>) list.getSymptoms();
		
		//Sorting and counting the symptoms
	
		SymptomCounter count = new SymptomCounter(result);
	
		Map<String, Integer> hmresult = count.countSymptoms();
		
		//Writing the result on a file
		
		WriteSymptomResult writefile = new WriteSymptomResult(hmresult, "results.txt");
		
		writefile.createFileResult();
	
	}
	
}
