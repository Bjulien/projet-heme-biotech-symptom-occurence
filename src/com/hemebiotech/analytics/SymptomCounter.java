package com.hemebiotech.analytics;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class SymptomCounter {
	
	private ArrayList<String> list;
	// List containing all the symptoms
	
	public SymptomCounter (ArrayList<String> list) {
		this.list = list;
	}

	public Map<String, Integer> countSymptoms() {
		 // HashMap to store the frequency of each symptoms
        Map<String, Integer> counthm = new TreeMap<String, Integer>();
  
        for (String symptom : list) {
            Integer occurence = counthm.get(symptom);

            if (occurence == null) {
            	occurence = 1;
            }
            else {
            	occurence++;
            }
            counthm.put(symptom, occurence);
        }
  
        // displaying the occurrence of symptoms (for testing)
        
        /*
        for (Map.Entry<String, Integer> val : counthm.entrySet()) {
            System.out.println("Symptom " + val.getKey() + " "
                               + "occurs"
                               + ": " + val.getValue() + " times");
        }*/
        //return HashMap
        return counthm;
	}
	
	
}
