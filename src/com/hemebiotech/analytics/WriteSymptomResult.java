package com.hemebiotech.analytics;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class WriteSymptomResult {
	
	//take hmap and filepath
	
	private Map<String, Integer> map;
	private String filepath;
	
	public WriteSymptomResult (Map<String, Integer> map, String filepath) {
		this.map = map;
		this.filepath = filepath;
	}
	public void createFileResult() {
		
		//New file and writer
		File file = new File(filepath);
		BufferedWriter writer = null;
		
		try {
			writer = new BufferedWriter(new FileWriter(file));
			
			for (Map.Entry<String, Integer> entry : map.entrySet()) {
				
			    //Write Symptom + number of occurrence 
				writer.write(entry.getKey() + " : " + entry.getValue());
				
				// new line
				
				writer.newLine();
				
			}
			
			writer.flush();
			
		}
		catch (IOException e) {
            e.printStackTrace();
        }
		finally {
			
			try {
				//close writer
				writer.close();
			}
			catch (Exception e) {
            }
		}
	}
}
